Tamed Cargo
======================
This is the parcel and project dependency manager for the Tamed Framework.

license
=============
This project is licensed under the GNU AGPL version 3 or later.
