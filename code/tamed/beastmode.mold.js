'use strict';

// Object mold
var o = {};

o.__info = {
	enumerable: tm.enumerable,
	value: {
		needs: [
			'tamed/modus/object'
		]
	}
}

// Private

// Protected

// Public
o.public = {
	beastmode: {
		enumerable: tm.enumerable,
		value: function () {
			console.log ('*** WHAT');
		}
	}
}

// Javascript compatability
if (global !== undefined) {
	// Node
	exports.mold = o;
}
else {
	// Browser
	return o;
}


