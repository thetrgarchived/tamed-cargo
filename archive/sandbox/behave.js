
/*
- 'Beast Mode' can:  (Project perspective, "What can the project do?")
	- Story: create a new project 	("This is very broad. Further stories needed to get speicifics")
		- As a: Developer
		- I want to: create new projects from the command line
		- Senarios:
			- Senario: Create a brand new project base project
				- Given: I am in the directory for projects
				- When: I type in 'beastmode create testproject'
					- Spec: beastmode starts by invoking 'beastmode' in the command line
					- Spec: beastmode accepts 'create' as a command
						- Spec: This command accepts a project name as a parameter
				- And: there are no pre-existing projects with the desired name
					- Spec: Make sure the project name does not already exist
					- Spec: Generate error on pre-existing project with same desired name.
				- Then: the current directory
					- Spec: Make sure we are in the projects directory
				- Should: have a directory called 'testproject'
					- Spec: Make sure the project folder was created with desired name.
				- With a: file and folder structure similar to the default beastmode structure.
					- Spec: Make files and folders reference the default beastmode structure.
			- Senario: Set up a brand new project
				- Given: I am in the directory for a new project
				- When: I type 'beastmode update'
				- And: the 'catalog.json' file exists
					- Spec: Check to make sure the 'catalog.json' file exists.
					- Spec: Generate error on the 'catalog.json' file not existing.
				- And: changes have been made to the 'catalog.json' file
					- Spec: Compare the 'lastUpdate' property in the 'catalog.json' file to the file timestamp.
					- Spec: Generate message 'Project up to date.' on no difference in 'lastUpdate' property and file timestamp.
				- Then: open the 'catalog.json' file
				- And: Update project dependencies.
					- Story: Update project dependencies.
						- As a:
						- I want to:
						- Senarios:
*/

/*
var story = {
	story: 'Beastmode can create and manage project dependencies.',
	asA: 'Developer',
	iWantTo: 'create and manage project dependencies from the command line.',
	scenarios: [
		{
			senario: 'Create a brand new project.'
		},
		{
			senario: 'Update a project\'s dependencies.'
		},
	]
}
*/

require('tamed/modus/tamed.mold')(__dirname);

global.log = function log (message, o) {
	console.log (message);
}

var o = this;

o.runStory = function (o) {
	log ('Story: ' + o.story, o);
}

o.run = o.runStory;

o.runStories = function (o) {
	var key;
	var results;
	var stories;
	var story;
	var value;
	var behave;
	var test;
	var specs;
	var spec;

	results = o.results;
	stories = o.stories;

	var isNotNull = function () {
	}

	var specTest = {
		hasValue: function (result) {
			if (result == null ||
				result == undefined) {
				log ('Value is null');
			}
		}
	}

	/*function () {
		var o = this;

		o.spec = {};

		o.shouldNotBeNull = function () {
		}
	}
	*/

	specs = [
		function (spec) {
			// Setup the spec test.
			var name = 'beastmode starts by typing "beastmode" into the command line.';
			var result;

			// Spec test code.


			// Spec test results.
			spec.hasValue (result);
		}
	]

	for (key in specs) {
		spec = specs [key];
		spec (specTest);
	}

}

o.results = {
};

var story = function () {

}

o.stories = {
	test: '',
}

log ('- Running Behave stories...\n');
o.runStories (o);
log ('\n- All done.\n');

	/*
	stories = [
		{
			o: this,
			story: 'Beastmode can create and manage project dependencies.',
			asA: 'Developer',
			iWantTo: 'create and manage project dependencies from the command line.',
			scenarios: [
				{
					story: o,
					scenario: 'Create a brand new project.',
						{
							given: 'I am in the directory for projects.',

						},
						{
							when: 'I type in "beastmode create testproject"',
						}
				},
			]
		}
	]

	for (key in stories) {
		//results =
		story = stories [key];
		o.run (story);


	//	log ('stoy: %j', stories);
	//	o.run (story);

		//log ('\t- Story: ' + test.story);
	}
	*/
/*
- Given: I am in the directory for projects
				- When: I type in 'beastmode create testproject'
					- Spec: beastmode starts by invoking 'beastmode' in the command line
					- Spec: beastmode accepts 'create' as a command
						- Spec: This command accepts a project name as a parameter
				- And: there are no pre-existing projects with the desired name
					- Spec: Make sure the project name does not already exist
					- Spec: Generate error on pre-existing project with same desired name.
				- Then: the current directory
					- Spec: Make sure we are in the projects directory
				- Should: have a directory called 'testproject'
					- Spec: Make sure the project folder was created with desired name.
				- With a: file and folder structure similar to the default beastmode structure.
					- Spec: Make files and folders reference the default beastmode structure.
*/
