'use strict';

// Object mold
var o = {};

o.__info = {
	enumerable: tm.enumerable,
	value: {
		needs: [
			'tamed/modus/object'
		]
	}
}

// Private

// Protected

// Public
o.public = {
	start: {
		enumerable: tm.enumerable,
		value: function (o) {
			var key;
			var results;
			var stories;
			var story;
			var value;
			var behave;
			var test;
			var specs;
			var spec;

			results = o.results;
			stories = o.stories;

			var behave = tm.copy ('behave');

			var stories = [
				{
					story: "Cargo can create and manage project dependencies.",
					asA: "Developer",
					iWantTo: "create and manage project dependencies from the command line.",
					details: [
						{
							scenario: "Create a brand new project.",
							details: [
								{given: "I am in the directory for projects"},
								{when: "I type in 'cargo create testproject'",
									details: [
										{
											specs: 'cargo',
											details: [
												{
													name: "starts by invoking 'cargo' in the command line",
													spec: function (behave) {
														var bob = 5;

														behave.hasValue (bob);
													}
												},
												{
													name: "accepts 'create' as a command",
													spec: function (behave) {
													}
												},
												{
													name: "command 'create' accepts a 'project name' as a parameter",
													spec: function (behave) {
													}
												}
											]
										},
										{
											units: 'cargo',
											details: [
											]
										}
									]
								},
								{and: "there are no pre-existing projects with the desired name"},
								{then: "the current directory"},
								{should: "have a directory called 'testproject'"},
								{with: "a file and folder structure similar to the default cargo new project template"}
							]
						}
					]
				}
			];

			behave.startTests (behave, stories);
		}
	}
}

// Javascript compatability
if (global !== undefined) {
	// Node
	exports.mold = o;
}
else {
	// Browser
	return o;
}


