


story: "Cargo can create and manage project dependencies.",
asA: "Developer",
iWantTo: "create and manage project dependencies from the command line.",
scenarios: [
	{
		senario: "Create a brand new project.",
		details: [
			{given: "I am in the directory for projects"},
			{when: "I type in 'cargo create testproject' ",
				specs: [
					{cargo: "starts by invoking 'cargo' in the command line"},



var story = '';
{
	story: "Cargo can create and manage project dependencies.",
	asA: "Developer",
	iWantTo: "create and manage project dependencies from the command line.",
	scenarios: []
}





function (when) {
	when = "I type in ";

	specs = [
		function (spec) {
			spec.name = "cargo accepts 'create' as a command";
		}
	]
}

	stories = {};


	stories ["Cargo can create and manage project dependencies."] = {};
	story = stories ["Cargo can create and manage project dependencies."] = {};
		asA: "Developer",
		iWantTo: "create and manage project dependencies from the command line.",
		scenarios:
	}





{
	story: "Cargo can create and manage project dependencies.",
	asA: "Developer",
	iWantTo: "create and manage project dependencies from the command line.",
	run: function () {
	},
	scenarios: [
		{
			senario: "Create a brand new project.",
			details: [
				{given: "I am in the directory for projects"},
				{when: "I type in 'cargo create testproject' ",
					specs: [
						{cargo: "starts by invoking 'cargo' in the command line"},

}

o ["cargo starts by invoking 'cargo' in the command line"] = function (spec) {
}

o ["Cargo can create and manage project dependencies."] = function (story) {
	story = "";
}

function (spec) {
	spec
}

function (spec) {
	story = "cargo accepts 'create' as a command";
	story.asA = "cargo accepts 'create' as a command";
	story.iWantTo = "cargo accepts 'create' as a command";

	story.scenarios: [
	]
}

function (spec) {
	spec.name = "cargo accepts 'create' as a command";
}


{
	senario: "Update a project's dependencies.",
	details: [
		{given: "I am in the directory for projects"},
		{when: "I type in 'cargo update' "},
		{and: "the 'catalog.json' file exists"},
		{and: "changes have been made to the 'catalog.json' file"},
		{then: "open the 'catalog.json' file"},
		{and: "upd the project dependencies."}
	]
},



{
	senario: "Create a brand new project.",
	details: [
		{given: "I am in the directory for projects"},
		{when: "I type in 'cargo create testproject' ",
			specs: [
				function (spec) {
					spec.name = "cargo starts by invoking 'cargo' in the command line";
				},

				function (spec) {
					spec.name = "cargo accepts 'create' as a command";
				},

				function (spec) {
					spec.name = "cargo command 'create' accepts a 'project name' as a parameter";
				}
			]
		},
		{and: "there are no pre-existing projects with the desired name"},
		{then: "the current directory"},
		{should: "have a directory called 'testproject'"},
		{withA: "file and folder structure similar to the default cargo new project template"}
	]
},

stories = [
	{
		story: "Cargo can create and manage project dependencies.",
		asA: "Developer",
		iWantTo: "create and manage project dependencies from the command line.",
		scenarios: [
			{
				senario: "Create a brand new project.",
				details: [
					{given: "I am in the directory for projects"},
					{when: "I type in 'cargo create testproject' ",
						specs: [
							{cargo: "starts by invoking 'cargo' in the command line"},
							{cargo: "accepts 'create' as a command"},
							{cargo: "command 'create' accepts a 'project name' as a parameter"}
						]
					},
					{and: "there are no pre-existing projects with the desired name"},
					{then: "the current directory"},
					{should: "have a directory called 'testproject'"},
					{withA: "file and folder structure similar to the default cargo new project template"}
				]
			},
			{
				senario: "Update a project's dependencies.",
				details: [
					{given: "I am in the directory for projects"},
					{when: "I type in 'cargo update' "},
					{and: "the 'catalog.json' file exists"},
					{and: "changes have been made to the 'catalog.json' file"},
					{then: "open the 'catalog.json' file"},
					{and: "upd the project dependencies."}
				]
			},
		]
	}
]

stories = [];


	{
		story: "Cargo can create and manage project dependencies.",
		asA: "Developer",
		iWantTo: "create and manage project dependencies from the command line.",
		scenarios: [
			{
				senario: "Create a brand new project.",
				details: [
					{given: "I am in the directory for projects"},
					{when: "I type in 'cargo create testproject'",
						specs: [
							cargo: [
								function (spec) {
									spec.name = "starts by invoking 'cargo' in the command line";
								},

								function (spec) {
									spec.name = "accepts 'create' as a command";
								},

								function (spec) {
									spec.name = "command 'create' accepts a 'project name' as a parameter";
								}
							]
						]
					},
					{and: "there are no pre-existing projects with the desired name"},
					{then: "the current directory"},
					{should: "have a directory called 'testproject'"},
					{withA: "file and folder structure similar to the default cargo new project template"}
				]
			}
		]
	}

















