'use strict';

// Object mold
var o = {};

o.__info = {
	enumerable: tm.enumerable,
	value: {
		needs: [
			'tamed/modus/object'
		]
	}
}

// Private

// Protected

// Public
o.public = {
	stats: {
		enumerable: tm.enumerable,
		value: {
			total: 0,
			pass: 0,
			fail: 0,
			notImplemented: 0,
			final: ''
		}
	},

	hasValue: {
		enumerable: tm.enumerable,
		value: function (result) {
			var o = this;

			if (result == null ||
				result == undefined) {
				// Fail the test.
				o.tempStats.fail += 1;
			}
			else {

				// Pass the test.
				o.tempStats.pass += 1;
			}
		}
	},

	setSessionName: {
		enumerable: tm.enumerable,
		value: function (o, name) {
			if (o === undefined) {o = this;}

			o.__info.log.name = '\x1B[48;5;17m' + '\t ' + name + '\t \x1B[0m';
		}
	},

	showReport: {
		enumerable: tm.enumerable,
		value: function (o) {
			if (o === undefined) {o = this;}

			var color;
			var message;

			log (o, '\n\n', false, '\t');
			log (o, '\x1B[48;5;22m' + '\t Passed:\t\t' + o.stats.pass + '\t \x1B[0m');
			log (o, '\x1B[48;5;88m' + '\t Failed:\t\t' + o.stats.fail + '\t \x1B[0m');
			log (o, '\x1B[48;5;94m' + '\t Not Implemented:\t' + o.stats.notImplemented + '\t \x1B[0m');
			log (o, '');

			if (o.stats.fail > 0) {
				color = '\x1B[48;5;124m';
				message = '\t FINAL: FAIL - several tests failed.';
			}
			else if (o.stats.notImplemented > 0) {
				color = '\x1B[48;5;124m';
				message = '\t FINAL: FAIL - several tests not implemented.';
			}
			else {
				color = '\x1B[48;5;28m';
				message = '\t FINAL: PASS ';
			}

			log (o, color + message + '\t \x1B[0m');
			log (o, '', true);
		}
	},

	startTests: {
		enumerable: tm.enumerable,
		value: function (o, stories) {
			if (o === undefined) {o = this;}

			var story;
			var key;

			for (key in stories) {
				story = stories [key];
				o.runTest (o, story);
			}
		}
	},

	runSpec: {
		enumerable: tm.enumerable,
		value: function (o, spec) {
			if (o === undefined) {o = this;}

			var prefix;

			prefix = '';
			o.tempStats = {
				pass: 0,
				fail: 0
			}

			// Run the spec.
			try {
				spec.spec (o);
			}
			catch (e) {
			}

			// Process the spec results.
			if (o.tempStats.pass == 0 &&
				o.tempStats.fail == 0) {
				o.stats.notImplemented += 1;
				prefix += '\x1B[48;5;94m' + ' WIP: ' + '\x1B[0m';
			}
			else if (o.tempStats.fail > 0) {
				o.stats.fail += 1;
				prefix += '\x1B[48;5;88m' + ' FAIL: ' + '\x1B[0m';
			}
			else {
				o.stats.pass += 1;
				prefix += '\x1B[48;5;22m' + ' PASS: ' + '\x1B[0m';
			}

			// Display the spec results.
			log (o, spec.name, false, '\t\t\t ' + prefix + ' ');
		}
	},

	runTest: {
		enumerable: tm.enumerable,
		value: function (o, test) {
			o = this;
			var session;
			var senarios;

			var key;
			var value;
			var type;
			var test;
			var childTests;
			var name;
			var word;
			var prefix;
			var spec;
			var specs;

			// Store the types of story key words we can use.
			var keyWords = [
				'story', 'scenario', 'given', 'when', 'and',
				'should', 'with', 'specs', 'name'
			];

			for (key in keyWords) {
				word = keyWords [key];
				name = test [word];

				if (name !== undefined) {
					prefix = '\t    \x1B[48;5;236m ';

					switch (word) {
						case 'story':
							session = tm.copy ('behave');
							session.setSessionName (session, word + ': ' + test.story);
							break;

						case 'scenario':
							break;

						case 'given':
							break;

						case 'when':
							break;

						case 'and':
							break;

						case 'should':
							break;

						case 'with':
							break;

						case 'specs':
							break;

						case 'name':
							break;
					}

					if (session === undefined) {
						if (word == 'specs') {
						//	session = tm.copy ('behave');
						//	session.setSessionName (session, word + ': ' + name);
							log (o, 'specs: ' + name, false, '\t\t');

							specs = test.details;
							for (key in specs) {
								spec = specs [key];
								o.runSpec (o, spec);
							}
						}
						else {
							log (o, word + ':\t' + '\x1B[0m ' + name, false, prefix);
							o.startTests (o, test.details);
						}
					}
					else {
						session.startTests (session, test.details);
						session.showReport (session);
					}

					break;
				}
			}
		}
	}
}

// Javascript compatability
if (global !== undefined) {
	// Node
	exports.mold = o;
}
else {
	// Browser
	return o;
}


