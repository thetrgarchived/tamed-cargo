'use strict';

// Object mold
var o = {};

o.__info = {
	enumerable: tm.enumerable,
	value: {
		needs: [
			'tamed/modus/object'
		]
	}
}

// Private

// Protected

// Public
o.public = {
	stats: {
		enumerable: tm.enumerable,
		value: {
			total: 0,
			pass: 0,
			fail: 0,
			notImplemented: 0,
			final: ''
		}
	},

	hasValue: {
		enumerable: tm.enumerable,
		value: function (result) {
			var o = this;

			if (result == null ||
				result == undefined) {
				//log (null, 'Value is null');

				// Fail the test.
				log (o, '\x1B[48;5;88m' + '\t FAILURE: ' + o.name + ' \t \x1B[0m');
				o.tempStats.fail += 1;
			}
			else {

				// Pass the test.
				o.tempStats.pass += 1;
			}
		}
	},

	setSessionName: {
		enumerable: tm.enumerable,
		value: function (o, name) {
			var o = this;
			o.__info.log.name = '\x1B[48;5;25m' + '\t ' + name + '\t \x1B[0m';
		}
	},

	showReport: {
		enumerable: tm.enumerable,
		value: function (o) {
			var o = this;
			var color;
			var message;

			log (o, '');
			log (o, '\x1B[48;5;22m' + '\t Passed:\t\t' + o.stats.pass + '\t \x1B[0m');
			log (o, '\x1B[48;5;88m' + '\t Failed:\t\t' + o.stats.fail + '\t \x1B[0m');
			log (o, '\x1B[48;5;94m' + '\t Not Implemented:\t' + o.stats.notImplemented + '\t \x1B[0m');
			log (o, '');

			if (o.stats.fail > 0) {
				color = '\x1B[48;5;124m';
				message = '\t FINAL: FAIL - several tests failed.';
			}
			else if (o.stats.notImplemented > 0) {
				color = '\x1B[48;5;124m';
				message = '\t FINAL: FAIL - several tests not implemented.';
			}
			else {
				color = '\x1B[48;5;28m';
				message = '\t FINAL: PASS ';
			}

			log (o, color + message + '\t \x1B[0m');

			log (o, '', true);
		}
	},

	runTest: {
		enumerable: tm.enumerable,
		value: function (test) {
			var o = this;
			var result;

			o.stats.total += 1;

			// Reset the test.
			o.name = '';
			o.results = null;
			o.tempStats = {
				pass: 0,
				fail: 0
			}

			// Run the test.
			result = test (o);

			if (o.tempStats.pass == 0 &&
				o.tempStats.fail == 0) {
				o.stats.notImplemented += 1;

				log (o, 'Not implemented: ' + o.name);
			}
			else if (o.tempStats.fail > 0) {
				o.stats.fail += 1;
			}
			else {
				o.stats.pass += 1;
			}

			/*
			switch (result) {
				case true:
					o.stats.pass += 1;
					break;

				case false:
					o.stats.fail += 1;
					break;

				default:
					o.stats.notImplemented += 1;
			}
			*/
		}
	}
}

// Javascript compatability
if (global !== undefined) {
	// Node
	exports.mold = o;
}
else {
	// Browser
	return o;
}


