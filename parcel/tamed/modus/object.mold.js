'use strict';

// Object mold
var o = {};

o.__info = {
	enumerable: tm.enumerable,
	value: {
		needs: [
		]
	}
}

// Private
o.private = {
	getId: {
		enumerable: tm.enumerable,
		value: function (o, vars, funcs, args) {
			if (o === undefined) {o = this};
			return vars.id;
		},
	},

	__object: {
		enumerable: tm.enumerable,
		value: function (object, namespace) {
			// local variables.
			var o = this;
			var fullType;
			var functions;
			var moldFunctions;
			var variables;

			// Set up local variables.
			o = this;
			fullType = object.__info.fullType;
			moldFunctions = tm.molds [fullType] ['__private'] .value [namespace];

			// Set up the area's private variables.
			var variables = {
				id: tm.generateObjectId ()
			}

			// Set up the area's private functions.
			var functions = {
				getId: moldFunctions.getId.value
			}

			// A public fowarding function to access properly assigned
			// private functions of this private area.
			o.__call = function (funcName, args) {
				if (args === undefined) {args = {}};
				return functions [funcName] (o, variables, functions, args);
			}

			return o;
		}
	}
}

// Protected

// Public
o.public = {
	logHistory: {
		enumerable: tm.enumerable,
		writable: true,
		value: '',
	},

	getId: {
		enumerable: tm.enumerable,
		value: function () {
			return this.__private._tamed_modus_object.__call ('getId');
		}
	},

	toString: {
		enumerable: tm.enumerable,
		value: function () {
			return JSON.stringify(this, function (key, value) {
					if (typeof value === 'function') {
						return 'Function'  //value.toString();
					}
					return value;
				}, '\t');
		}
	}
}

// Javascript compatability
if (global !== undefined) {
	// Node
	exports.mold = o;
}
else {
	// Browser
	return o;
}
