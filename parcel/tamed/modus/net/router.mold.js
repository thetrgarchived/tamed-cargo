'use strict';

// Object mold
var o = {};

o.__info = {
	enumerable: tm.enumerable,
	value: {
		needs: [
			'tamed/modus/object',
		]
	}
}

// Private

// Protected

// Public
o.public = {
	routeFromPath: {
		enumerable: tm.enumerable,
		value: function (o, http, request, response) {
			if (o === undefined) {o = this};

			o.STATUS = tm.molds ['tamed/modus/net/httpServer'].STATUS.value;

			var content;
			var contentType;
			var encoding;
			var fs;
			var func;
			var object;
			var path;
			var pathInfo;
			var periodIndex;
			var publicPath;
			var resourcePath;
			var responseObject;
			var routePath;
			var slashIndex;
			var url;

			o.historyLog = '';

			// Get the path information.
			url = require ('url');

			path = url.parse (request.url);

			// Clean up the request path to get the path formated the
			// way tamed routes need to be.
			periodIndex = -1;
			slashIndex = -1;
			slashIndex = path.pathname.lastIndexOf ('/');
			periodIndex = path.pathname.lastIndexOf ('.');

			path.pathname = path.pathname.substring (1, path.pathname.length);
			path.path = path.path.substring (1, path.path.length);
			path.href = path.href.substring (1, path.href.length);

			// Check if path is looking for a directory or resource.
			if (slashIndex > periodIndex) {
				// A slash was found at the far end of path before a
				// period was. This means path is looking for a
				// directory.
				if (slashIndex < (path.pathname.length - 1)) {
					path.pathname = path.pathname + '/';
					path.path = path.path + '/';
					path.href = path.href + '/';
				}

				path.extension = 'object';
			}
			else {
				// Set the resource extension.
				path.extension = path.path.substring (path.path.lastIndexOf ('.') + 1, path.path.length);
			}

			// Log that a request was recieved.
		//	log (o, '--------------');
			log (o, 'request: ');
			log (o, 'request path: ' + path.pathname, false, '\t\t- ');

			routePath = tm.config.main; //o.paths [path.pathname];
			//log ('path: ' + tm.jsonStr (routePath.path));


			switch (path.extension) {
				case 'png':
				case 'ico':
				case 'jpg':
				case 'gif':
					contentType = 'image/' + path.extension;
					break;

				case 'ttf':
					contentType = 'font/opentype';
					contentType = 'font/ttf .ttf';
					break;

				case 'woff':
					contentType = 'application/font-' + path.extension;
					contentType = 'font/opentype';
					contentType = 'font/woff .woff';
					break;

				case 'js':
					contentType = 'application/javascript';
					break;

				case 'css':
					contentType = 'text/css';
					break;

				case 'object':
					contentType = 'text/html';
					break;
			}

			if (path.extension == 'object') {
				if (routePath === undefined) {
					responseObject = {
						code: {value: o.STATUS.NOT_FOUND.code, msg: o.STATUS.NOT_FOUND.msg},
						header: {'Content-Type': 'text/html'},
						content: o.STATUS.NOT_FOUND.msg,
						errors: []
					}
				}
				else {
					// Set up the object to process the response.
					try {

						/*
						if (routePath.func !== undefined) {
							func = routePath.func;
						}
						else {
							pathInfo = tm.getPathInfo (routePath.object);
							func = pathInfo.name;
						}

						object.STATUS = tm.molds ['tamed/modus/net/httpServer'].STATUS.value;
						*/
						//log (tm.jsonStr (object.site ({response: response, func: routePath.func}, object)));
						//responseObject = object [pathInfo.name] ({response: response, func: routePath.func}, object);
						//responseObject = object.site (path);

						object = tm.copy (tm.config.main);

						pathInfo = tm.getPathInfo(tm.config.main);
						path.parts = path.pathname.split('/');

						responseObject = object [pathInfo.name] (path);
					}
					catch (e) {
						responseObject = {
							code: {value: o.STATUS.NOT_FOUND.code, msg: o.STATUS.NOT_FOUND.msg},
							header: {'Content-Type': 'text/plain'},
							content: o.STATUS.NOT_FOUND.msg,
							errors: []
						}

						error (o, 'Unable to find object: ' + path.pathname);
						error (o, 'stack trace: ' + e.trace);
					}


					// Log any errors while trying to use the object.
					if (responseObject.errors.length > 0) {
						for (var error in responseObject.errors) {
							error (o, responseObject.errors [error]);
						}
					}

					// Make sure the response object is not null.
					if (responseObject === undefined) {
						throw 'what the flip';
					}

					// Write out the response.
					response.writeHead (responseObject.code.value, responseObject.header);
					response.end (responseObject.content);
				}
			}
			else {
				contentType = 'text/';
				encoding = 'binary';
				publicPath = tm.__info.publicPath;
				resourcePath = publicPath + path.pathname;

				try {
					fs = require ('fs');
					content = '';
					content = fs.readFileSync (resourcePath);

					if (path.extension == 'js' || path.extension == 'css') {
						content = content.toString ();
						encoding = 'UTF-8';
					}

					responseObject = {
						code: {value: o.STATUS.OK.code, msg: o.STATUS.OK.msg},
						header: {'Content-Type': contentType + path.extension},
						content: content
					}
				}
				catch (e) {
					responseObject = {
						code: {value: o.STATUS.NOT_FOUND.code, msg: o.STATUS.NOT_FOUND.msg},
						header: {'Content-Type': 'text/plain'},
						content: o.STATUS.NOT_FOUND.msg
					}

					error (o, 'Unable to load the file: ' + resourcePath);
					error (o, 'stack trace: ' + e.trace);
				}

				response.writeHead (responseObject.code.value, responseObject.header);
				response.end (responseObject.content, encoding);
			}

			log (o, 'response code: ' + responseObject.code.value);
			log (o, 'response code msg: ' + responseObject.code.msg);
			log (o, 'response time: ' + 0 + ' ms');
			log (o, true);
		}
	}
}

// Javascript compatability
if (global !== undefined) {
	// Node
	exports.mold = o;
}
else {
	// Browser
	return o;
}

