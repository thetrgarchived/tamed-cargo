'use strict';

// Object mold
var o = {};

o.__info = {
	enumerable: tm.enumerable,
	value: {
		needs: [
			'tamed/modus/object',
		]
	}
}

// Private
o.private = {
	onRequest: {
		enumerable: tm.enumerable,
		value: function (o, vars, funcs, request, response, routerConfig) {
			if (o === undefined) {o = this};

			var router;

			// Route the request to the appropriate path object.
			router = tm.copy ('tamed/modus/net/router');
			router.paths = routerConfig;
			router.routeFromPath (router, o, request, response);
		}
	},

	startServer: {
		enumerable: tm.enumerable,
		value: function (o, vars, funcs, args) {
			if (o === undefined) {o = this};

			if (global !== undefined) {
				var http;
				var routerConfig;
				var server;

				// Create the http server.
				http = require ('http');

				/*
				if (tm.config.httpRouter == null) {
					o.error (o, 'Unable to load the routing settings. The routing settings are not defined in the \'catalog.json\' file.', true);
				}
				else {
					if (tm.config.httpRouter.paths === undefined) {
						o.error (o, 'Unable to set up routing paths. The routing paths are not defined in the \'catalog.json\' file.', true);
					}

					routerConfig = tm.config.httpRouter.paths;
				}
				*/

			//	global.error = o.error;
			//	global.log = o.log;

				server = http.createServer (function (request, response) {
					funcs.onRequest (o, vars, funcs, request, response, routerConfig);
				}).on ('error', function(e) {
					error (o, 'Unable to start server on port \'' + data.port + '\'. Port already in use.');
					error (o, e.message, true);
				});

				// Start the http server.
				server.listen (vars.port);

				log (o, 'Tamed http server has started on port \'' + vars.port + '\'. Press Ctrl + C to shutdown the server.', true);
			}
			else {
				error (o, 'Cannot use http server in a browser/client side.');
			}
		}
	},

	__object: {
		enumerable: tm.enumerable,
		value: function (object, namespace) {
			// local variables.
			var o = this;
			var fullType;
			var functions;
			var moldFunctions;
			var variables;
			var STATUS;

			// Set up local variables.
			o = this;

			o.logHistory = '';

			fullType = object.__info.fullType;
			moldFunctions = tm.molds [fullType] ['__private'] .value [namespace];

			// Set up the area's private variables.
			variables = {
				port: 8080
			}

			// Set up the area's private functions.
			functions = {
				onRequest: moldFunctions.onRequest.value,
				startServer: moldFunctions.startServer.value
			}

			STATUS = object.STATUS;
			o.STATUS = STATUS;

			// A public fowarding function to access properly assigned
			// private functions of this private area.
			o.__call = function (funcName, args) {
				if (args === undefined) {args = {}};
				return functions [funcName] (o, variables, functions, args);
			}

			return o;
		}
	}
}

// Protected

// Public
o.public = {
	// Ref: http://www.w3schools.com/tags/ref_httpmessages.asp
	STATUS: {
		enumerable: tm.enumerable,
		value: {
			OK: {code: 200, msg: 'The request is OK.'},
			NOT_FOUND: {code: 404, msg: 'The requested page could not be found but may be available again in the future.', file: ''},
		}
	},

	startServer: {
		enumerable: tm.enumerable,
		value: function () {
			return this.__private._tamed_modus_net_httpServer.__call ('startServer');
		}
	}
}

// Javascript compatability
if (global !== undefined) {
	// Node
	exports.mold = o;
}
else {
	// Browser
	return o;
}
