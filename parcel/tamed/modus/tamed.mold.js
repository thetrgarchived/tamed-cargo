'use strict';

module.exports = function (rootPath) {
	var tamed = {};
	var tm = tamed;
	var dir;
	var baseItems = {};
	var molds = {};
	var content = {};
	var objectCount = 0;
	var privateMoldCount = 0;

	tm.configurable = true;
	tm.enumerable = true;
	tm.writable = true;

	tm.logHistory = '';
	tm.errorHistory = '';
	tm.errorLog = '';
	tm.ERROR = '*** ERROR: ';
	tm.mode;

	var jsLoad = function (name) {
	}

	var getPathInfo = function (name) {
		var path 		= {};
		var lastIndex 	= name.lastIndexOf ('/');
		path.name 		= name.substring (lastIndex + 1, name.length) + '';
		path.namespace 	= name.substring (0, lastIndex) + '/';
		path.mold 		= path.namespace; //+ '.mold/';

		// Make sure the path does not contain an extra trail slash.
		if (path.namespace.length == 1) {
			path.namespace = '';
		}

		if (path.mold.length == 1) {
			path.mold = '';
		}

		/*
		log (null, 'name: ' + path.name);
		log (null, 'namespace: ' + path.namespace);
		log (null, 'mold: ' + path.mold);
		*/

		return path;
	}

	var browserMoldLoad = function (name) {
	}

	var nodeMoldLoad = function (name) {
		var o = this;
		var message;
		var mold;
		var path;

		message = '';

	//	log ('\n------------------');
		log (o, 'loading mold: ' + name);
		path = tm.getPathInfo (name);
		path = path.mold + path.name + '.mold';

		// Look for package in the parcel path.
		try {
			mold = require (tm.__info.parcelPath + path);
			return mold.mold;
		}
		catch (e) {
			message += 'Unable to load file: ' + name + ' from the parcel path.' + '\n';
		}

		// Look for package in code path.
		try {
			mold = require (tm.__info.codePath + path);
			return mold.mold;
		}
		catch (e) {
			message += 'Unable to load file: ' + name + ' from the code path.' + '\n';
		}

		// Check if the package needs to be loaded from the tests.
		if (tm.__info.mode == 'test') {
			try {
				mold = require (tm.__info.testPath + path);
				return mold.mold;
			}
			catch (e) {
				message += 'Unable to load file: ' + name + ' from the test path.' + '\n';
				message += e.stack;
			}
		}

		error (o, message, true, true);
	}

	var getMold = function (name) {
		var info;
		var item;
		var index = 0;
		var loaded = {};
		var loadedMold;
		var mold;
		var namespace;
		var needs;
		var path;
		var privateObject;
		var prop;
		//var privateAreas = {};

		var privateArea;
		var protectedArea;
		var publicArea;
		var moldPrivate;


		path = tm.getPathInfo (name);
		//path = path.mold + path.name + '.mold';

		// ------------------------------------------
		// If the mold is not in the base molds we
		// need to build it.
		if (molds [name] === undefined) {
			mold = tm.moldLoad (name);

			mold.__info.value.type = path.name;
			mold.__info.value.namespace = path.namespace;
			mold.__info.value.fullType = path.namespace + path.name;
			mold.__info.value.mold = null;

			info = mold.__info.value;

			namespace = info.namespace + info.type;
			namespace = '_' + namespace.replace (/\//g, '_');

			mold.__info.value.moldNamspace = namespace;

			moldPrivate = {
				privateMoldId: 0,
				configurable: tm.configurable,
				enumerable: tm.enumerable,
				writable: tm.writable,
				value: {}
			};

			privateArea = mold.private;
			protectedArea = mold.protected;
			publicArea = mold.public;

			var privateNeeds = {};

			// ------------------------------------------
			// If the mold has any dependencies we need
			// to load them.
			if (mold.__info.value.needs.length > 0) {
				needs = mold.__info.value.needs;

				for (item in needs) {
					loadedMold = getMold (needs [item]);

					// Add the mold's private object.
					if (loadedMold.__private !== undefined) {

						namespace = loadedMold.__info.value.fullType;
						namespace = '_' + namespace.replace (/\//g, '_');

						privateNeeds [namespace] = loadedMold.__private.value [namespace];
					}

					// Set the other properties of the object.
					for(prop in loadedMold) {
						if (prop != '__private') {
							if (prop != '__info') {
								mold [prop] = loadedMold [prop];
							}
						}
					}

					loaded [needs [item]] = index;
		/*
		log ('name: ' + path.name);
		log ('name: ' + path.namespace);
		log ('name: ' + path.mold);
		*/
					index++;
				}
			}

			// ------------------------------------------
			// Set the other properties of the object.
			namespace = '_' + info.fullType.replace (/\//g, '_');

			// ------------------------------------------
			if (privateArea !== undefined) {

				if (privateArea.__object === undefined) {
					tm.error ('The mold \'' + namespace + '\' does not have the private object defined.');
				}

				privateNeeds [namespace] = {};

				for(prop in privateArea) {
					privateNeeds [namespace] [prop] = privateArea [prop];
				}

			}

			if (privateNeeds !== null) {
				moldPrivate.value = privateNeeds;
				mold.__private = moldPrivate;
				mold.__private.privateMoldId = (privateMoldCount = privateMoldCount + 1);
			}

			if (namespace == '_tamed_modus_net_router') {
			//	log ('');
			//	log ('privateNeeds: ' + tm.jsonStr (privateNeeds));
			//	tm.error ('End test...');
			}

			mold.private = null;
			delete mold.private;

			// ------------------------------------------
			for(prop in protectedArea) {
				mold [namespace + '__protected_' + prop] = protectedArea [prop];
			}

			mold.protected = null;
			delete mold.protected;

			// ------------------------------------------
			for(prop in publicArea) {
				mold [prop] = publicArea [prop];
			}

			mold.public = null;
			delete mold.public;

			// ------------------------------------------
			// Copy over the private aspsects of mold.
			//mold.__private = undefined;

			// Add the listing of loaded items to the
			for (item in loaded) {
				index = loaded [item];
				mold.__info.value.needs[index] = item;
			}

			// Set the object name and set as base mold.
			mold.__info.value = info;

			molds [name] = mold;
		}

		return molds [name];
	}

	var load = function (name) {
		var extension;
		var item;
		var mold;

		if (name.lastIndexOf ('.') > -1) {
			extension = name.substring (name.lastIndexOf ('.'), name.length);
		}
		else {
			extension = 'js';
		}

		//log('extension: ' + extension);

		// ------------------------------------------
		// Check the type of resource we are tyring
		// to load.
		switch (extension) {
			case 'png':
			case 'jpg':
			case 'gif':
				break;

			case 'js':
			default:
				item = getMold (name);
		}

		return item;
	}

	var copy = function (refObject, args) {
		var mold;
		var object;
		var logObject;

		if (args === undefined) {
			args = {};
		}

		if (args.log === undefined) {
			args.log = {
				name: '',
				history: '',
				prefix: '\t- '
			}
		}

		if (refObject.__info === undefined) {
			// We are copying an object by a string ref name.
		//	log('-----------');
			//mold = tm.load('tamed/modus/object');
			mold = tm.load(refObject);
		//	log('mold: ' + tm.jsonStr (mold));
		//	log('');
		}
		else {
			// We are copying an actual object.
			mold = refObject.__info.mold;
		}

		// Create the object copy based on the mold.
		object = Object.create(null, mold);

		// Create the object's private space.
		if (mold.__private !== undefined) {
			var prop;
			var priv = mold.__private.value;
			Object.defineProperty (object, '__private', {configurable: true, enumerable: true, writable: true, value: {}});

			for (prop in priv) {
				/*
				log ('** mold: ' + tm.jsonStr (mold));
				log ('** priv: ' + tm.jsonStr (priv));
				log ('** prop: ' + prop);
				log ('** priv [prop]: ' + tm.jsonStr (priv [prop]));
				*/

				if (priv [prop].__object === undefined) {
				//	tm.error (tm.jsonStr (priv));
				}

				object.__private [prop] = new priv [prop].__object.value (object, prop);

				// Set a reference to the object's
				object.__private [prop].__info = {
					log: args.log
				};
			}
		}

		// Set a reference to the object's mold.
		Object.defineProperty(object.__info, 'mold', {
			enumerable: false,
			value: mold
		});

		// Set a reference to the object's
		object.__info.log = args.log;

		//log ('object id: ' + object.getId (object));

		return object;
	}

	var generateObjectId = function (obj) {
		objectCount += 1;
		return objectCount;
	}

	tm.log = function (o, message, dump, prefix) {
		if (o == null) {
			console.log (message);
		}
		else {
			if (prefix === undefined) {
				prefix = '';
			}
			else {
				o.__info.log.prefix = prefix;
			}

			if (message.concat !== undefined) {
				o.__info.log.history = o.__info.log.history + o.__info.log.prefix + message + '\n';
			}
			else {
				dump = true;
			}

			if (dump == true) {
			//	console.log ('- Log History: \n' + o.logHistory);
				console.log ('-------------------------------');
				console.log ('- log: ' + o.__info.log.name);
				console.log ('' + o.__info.log.history);
				o.__info.log.history = '';
			}
		}
	}

	tm.error = function (o, message, dump, exit) {
		log (o, tm.ERROR + message, dump);

		/*
		o.logHistory = o.logHistory + '\t' + tm.ERROR + message + '\n';

		if (dump == true) {
			console.log ('*** ERROR History: \n' + o.logHistory);
			o.logHistory = '';
		}
		*/

		if (exit === true) {
			log (o, true);
			tm.exit (1);
		}

		/*
		if (exit === undefined) {exit = true;}

		tm.errorLog += errorMsg + '\n';

		if (exit == true) {
			log ('');
			log (tm.ERROR + errorMsg);
			log ('');
			tm.exit (1);
		}
		*/
	}

	// Allow for the premature exiting of an app.
	tm.exit = function (status) {
		if (tm.mode === 'node') {
			log (o, '');
			process.exit (status);
		}
		else {
			throw 'Ending program: ' + status;
		}
	}


	// Set up the javascript environment.
	if (global === undefined) {
		// Set up things for the browser.

		tm.mode = 'browser';
		tm.load = jsLoad;
	}
	else {
		// Set up tamed for node.
		tm.mode = 'node';

		var window = global;

		window.tamed 	= tm;
		window.tm 		= tm;
		window.log 		= tm.log; //console.log;
		window.error 	= tm.error;

		//tm.window = window;

		dir = __dirname;

		// Load the config file.
		var fs = require('fs');

		/*
		var pos 		= __dirname.lastIndexOf ('parcel/');
		var projectDir 	= __dirname.substring (0, pos);
		var parcelDir 	= __dirname.substring (0, pos) + 'parcel/';
		*/

		var projectDir 	= rootPath + '/';
		var parcelDir 	= rootPath + '/' + 'parcel/';
		var config;

		config = fs.readFileSync (projectDir + 'catalog.json').toString ();
		config = JSON.parse (config);

		tm.config = config;

		tm.moldLoad = nodeMoldLoad;
	}

	// Set up the object.
//	var lastIndex 	= dir.lastIndexOf ('parcel/');
//	var lastIndex 	= rootDir.lastIndexOf ('parcel/');
//	tm.__typeName 	= 'tamed';
//	tm.__type 		= 'tamed/modus/tamed';
//	tm.__rootPath 	= rootPath + '/'; //dir.substring (0, lastIndex);
//	tm.__publicPath = tm.__rootPath + 'public/';
//	tm.__codePath 	= tm.__rootPath + 'code/';
//	tm.__parcelPath = tm.__rootPath + 'parcel/';
//	tm.__moldPath 	= tm.__parcelPath + tm.__type + '/.mold/';

	//var rootPath = dir.substring (0, lastIndex);
	var tmType = 'tamed/modus/tamed';

	rootPath += '/';
	tm.__info = {
		typeName: 'tamed/modus/tamed',
		type: tmType,
		rootPath: rootPath,
		publicPath: rootPath + 'public/',
		testPath: rootPath + 'test/',
		codePath: rootPath + 'code/',
		parcelPath: rootPath + 'parcel/',
		moldPath: rootPath + 'parcel/' + tmType,  //+ '/.mold/'
		log: {
			name: '',
			history: '',
			prefix: '\t- '
		}
	}


	// Used in JSON stringfy to check for functions.
	tm.jsonStr = function (json) {
		return JSON.stringify (json, tm.funcStr, '\t');
	}

	tm.funcStr = function (key, value) {
		if (typeof value === 'function') {
			return 'Function'  //value.toString();
		}
		return value;
	}

	tm.getPathInfo = getPathInfo;
	tm.load = load;
	tm.generateObjectId = generateObjectId;
	tm.molds = molds;

	tm.logHistory = '';

	tm.copy = copy;

	var path;
	var startObject;
	var start;

	for (var i = 0; i < 1; i++) {
	//	start = tm.copy ('tamed/modus/net/router');

		switch (process.argv [2]) {
			case 'test':
				tm.__info.mode = 'test';
				start = 'start';
			//	start = tm.copy (config.test);
			//	start = tm.copy ('start');
			//	start.start (start);
				break;

			default:
				start = config.main;
			//	start = tm.copy (config.main);
		}

		startObject = tm.copy (start);
		log (tm, true);

		path = tm.getPathInfo (start);
		startObject [path.name] (startObject);

		//start.startServer ();
		//log (tm, true);
	}

	//log ('what: ' + tm.jsonStr(tm.molds));
}
