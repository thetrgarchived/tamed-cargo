#!/usr/bin/env node

function checkErrors (errors) {
	log ('ERRORS: %j', errors);
}

function createParentPaths (path) {
	var paths = path.split ('/');
	var i = 0;
	var end = paths.length - 1;
	var pathToMake = '';

	for (i = 0; i < end; i ++) {
		pathToMake += paths [i] + '/';

		try {
			fs.mkdirSync (pathToMake, '0755');
		}
		catch (e) {
			checkErrors (e);
		}
	}
}

function symLink (source, destination) {
	try {
		createParentPaths (destination);
		fs.symlinkSync (beast.relativePath + source, destination, 'dir');
	}
	catch (e) {
		checkErrors (e);
	}
}

function createReadMe () {
}



var log = console.log;
var fs = require ('fs');


var beast = {
	relativePath: '../../',
	paths: {
		tamed: 'thetabularasagroup/tamed/modus/parcel/tamed/'
	}
};

symLink ('tamed/modus/', 'parcel/bob/cool/tamed');
log ('project has been set up.\n');

/*
var config = fs.readFileSync ('catalog.json').toString ();
config = JSON.parse (config);

var beastmode = config.beastmode;
var project = beastmode.project;



log ('project name: %j', project);

var key;
var value;
for (key in project) {
	value = project [key];

	//fs.symlinkSync ('', '', '-s');

	if (value.concat !== undefined) {
		log ('value: ' + value);
	}
}
*/




